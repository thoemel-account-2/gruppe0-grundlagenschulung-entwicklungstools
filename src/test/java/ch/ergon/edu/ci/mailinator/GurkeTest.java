package ch.ergon.edu.ci.mailinator;

import org.junit.Assert;
import org.junit.Test;

public class GurkeTest {

    private static final int EVEN_NUMBER = 12;
    private static final int ODD_NUMBER = 11;
    private Gurke gurke = new Gurke();

    @Test
    public void evenNumberAsInput() {
        int nextBiggerEvenNumber = gurke.getNextBiggerEvenNumber(EVEN_NUMBER);

        Assert.assertEquals(EVEN_NUMBER + 2, nextBiggerEvenNumber);
    }

    @Test
    public void oddNumberAsInput() {
        int nextBiggerEvenNumber = gurke.getNextBiggerEvenNumber(ODD_NUMBER);

        Assert.assertEquals(ODD_NUMBER + 1, nextBiggerEvenNumber);
    }

}
