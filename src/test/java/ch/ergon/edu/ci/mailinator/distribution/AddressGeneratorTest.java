package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AddressGeneratorTest {

    private static final String ARETHA_CITY = "Winterthur";
    private static final int ARETHA_PLZ = 8400;
    private static final AddressEntry ADDRESS_ARETHA = new AddressEntry.Builder()
            .firstName("Aretha")
            .lastName("Franklin")
            .streetName("Ruhtalstrasse")
            .houseNumber("99")
            .plz(Integer.toString(ARETHA_PLZ))
            .city(ARETHA_CITY).build();

    private final AddressGenerator addressGenerator = new AddressGenerator();

    /**
     * Unit Test zu Aufgabe 1
     */
    @Ignore
    @Test
    public void letterMustContainFirstAndLastName() {
        Letter letter = new Letter();
        addressGenerator.insertAddress(letter, ADDRESS_ARETHA);
        Assert.assertThat(letter.getName(), Matchers.is("Aretha Franklin"));
    }

    /**
     * Unit Test zu Aufgabe 1
     */
    @Ignore
    @Test
    public void letterMustContainStreetAndNumber() {
        Letter letter = new Letter();
        addressGenerator.insertAddress(letter, ADDRESS_ARETHA);
        Assert.assertThat(letter.getStreet(), Matchers.is("Ruhtalstrasse 99"));
    }

    /**
     * Unit Test zu Aufgabe 1
     */
    @Ignore
    @Test
    public void letterMustContainCity() {
        Letter letter = new Letter();
        addressGenerator.insertAddress(letter, ADDRESS_ARETHA);
        Assert.assertThat(letter.getCity(), Matchers.is("Winterthur"));
    }

    /**
     * Unit Test zu Aufgabe 1
     */
    @Ignore
    @Test
    public void letterMustContainPlz() {
        Letter letter = new Letter();
        addressGenerator.insertAddress(letter, ADDRESS_ARETHA);
        Assert.assertThat(letter.getPlz(), Matchers.is(ARETHA_PLZ));
    }


}
