package ch.ergon.edu.ci.mailinator;

public class Gurke {

    public int getNextBiggerEvenNumber(int startingPoint) {
        if (startingPoint % 2 == 0) {
            return startingPoint + 2;
        } else {
            return startingPoint + 1;
        }
    }

}
