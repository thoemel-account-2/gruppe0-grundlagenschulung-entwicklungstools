package ch.ergon.edu.ci.mailinator.domain;

public class AddressEntry {
    private String firstName;
    private String lastName;
    private String streetName;
    private String houseNumber;
    private String plz;
    private String city;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static class Builder {
        private AddressEntry addressEntry = new AddressEntry();

        public Builder firstName(String firstName) {
            addressEntry.setFirstName(firstName);
            return this;
        }

        public Builder lastName(String lastName) {
            addressEntry.setLastName(lastName);
            return this;
        }

        public Builder streetName(String streetName) {
            addressEntry.setStreetName(streetName);
            return this;
        }

        public Builder houseNumber(String houseNumber) {
            addressEntry.setHouseNumber(houseNumber);
            return this;
        }

        public Builder city(String city) {
            addressEntry.setCity(city);
            return this;
        }

        public Builder plz(String plz) {
            addressEntry.setPlz(plz);
            return this;
        }

        public AddressEntry build() {
            return addressEntry;
        }
    }
}
